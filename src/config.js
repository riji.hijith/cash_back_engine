
const env = process.env.NODE_ENV || "dev"


const dev = {
  app: {
    PORT: 3001
  },
  sequelize: {
    host: "localhost",
    user: "root",
    password: "sahal_123#A",
    db_name: "cash_back_db",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  },
  mongoose: {
    MONGO_URL: "mongodb+srv://vijith:vijith123@cluster0.mso81.mongodb.net/cash_back_db?retryWrites=true&w=majority"
  }
}

const test = {
  app: {
    PORT: 3001
  },
  sequelize: {
    host: "localhost",
    user: "chillar_user",
    password: "chillar@123",
    db: "cash_back_db",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  },
  mongoose: {
    MONGO_URL: "mongodb+srv://vijith:vijith123@cluster0.mso81.mongodb.net/cash_back_db?retryWrites=true&w=majority"
  }

}

const config ={
  dev,
  test
}

export default config[env]