import user_table_services from '../db_services/user.table.services.js'
import param_table_services from '../db_services/param.table.services.js'



let user_services = {

    check_user_exsist: async ( /* userID must be an sting */ user_ID) => {
        try {

            return await user_table_services.get_user({ user_ID })
        } catch (error) {

        }
    },

    check_param_exsist:  (param) => {
        try {
            return  param_table_services.get_param({ param_Name: param })
        } catch (error) {
            throw error

        }
    }
}

export default user_services