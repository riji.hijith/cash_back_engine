const error_status = {
    success: {
        statusCode: 200,
        error: false,
        message: "sucessful"
    },
    bad_requset: {
        statusCode: 400,
        error: true,
        message: "Bad Requset"
    },
    server_error: {
        statusCode: 500,
        error: true,
        message: "Unable to process"

    }

}


const response = async (res, status, data, message) => {
    let { statusCode, error } = error_status[status]
    let msg = message || error_status[status].message
    return res.status(200).json({ statusCode, error, message, data })

}

export {
    response
}