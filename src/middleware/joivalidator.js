import Joi from "joi";
import moment from "moment";
let joiValidator = {

  createuser: async( req, res, next)=>{
        const userschema = Joi.object({
           
            system_ID: Joi.number(),
              externalUser_ID:Joi.string().required().messages({
                'string.base': `External ID should be a type of 'Number'`,
                'string.empty': `External ID cannot be an empty field`,
                'string.min': `External ID should have a minimum length of {#limit}`,
                'any.required': `External ID is a required field`,
                'string.pattern.base': `External ID should be valid.`
              }),
                 status: Joi.optional(),
                 userParams:Joi.optional()
         })
        let {
            error,
            value
        } = userschema.validate(req.body)
        if (error) {
            console.log(error.details[0].path);
            res.status(200).json({
                status:{
                    Error: true,
                    Code: 400,
                    Message:error.message
                }
             
            })
          
        } else if (value) {
            req.xop = value
            next()

        }

    },

    userParams: async( req, res, next)=>{
        const user_param_table = Joi.object({

        })
    }

    
}
export default joiValidator;