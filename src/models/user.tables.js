//** importing sequlize instanse from db config */
import { Sequelize, db, DataTypes } from "./../sequalize.connection.js"


const user = db.define("user_table", {

    user_ID: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    system_ID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true
        
      
    },

    externalUser_ID: {
        type: DataTypes.STRING,
        allowNull: false,
        unique  : true
      
    },
    status:{
        type: DataTypes.STRING,
        defaultValue:"active"
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
})



export default user