import { Sequelize, DataTypes, db } from "../sequalize.connection.js";

let user_param_table = db.define("user_param_table", {

    ID: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true

    },
    userID: {
        type: DataTypes.INTEGER,
        references: {
            model: 'user_tables',
            key: "user_ID"
        },
        allowNull: false,
    },
    paramID: {
        type: DataTypes.INTEGER,
        references: {
            model: 'param_tables',
            key: "param_ID"
        },
        allowNull: false,

    },
    value: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    status: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    },
    // type: {
    //     type: DataTypes.STRING,
    //     allowNull: false,
    // },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,

})

export default user_param_table