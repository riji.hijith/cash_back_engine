import { Sequelize, db, DataTypes } from "./../sequalize.connection.js"


const params = db.define("param_table",{
    param_ID: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true

    },
    param_Name : {
        type: DataTypes.STRING  ,
        allowNull: false,
       

    },
    // status :{
    //     type: DataTypes.STRING

    // },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,

})

export default params