async function commonResponse(res,code,message,data) {
    switch (code) {
      case 200:
        res.status(200).json({status: {Error: false,Code: code,Message: message || "Successful",data: data} });
        break;
      case 400:
      case 401:
        res
          .status(200)
          .json({
            status: {
              Error: true,
              Code: 401,
              Message: message || "Already Exist",
              data: data,
            },
          });
          break;
      case 403:
        res
          .status(200)
          .json({
            status: {
              Error: true,
              Code: code,
              Message: message || "Something went wrong",
            },
            data: data,
          });
        break;
      default:
        res
          .status(200)
          .json({
            status: { Error: true, Code: 500, Message: "Unable to process." },
          });
    }
}

  // async function commonResponse(res, code, message, data){

  //   return res.status(200).json({
  //     status: code,
  //     message: message,
  //     data: data,
  //   });
  // }

  async function failure_response(res, code, message) {
    return res.status(code).json({
      status: false,
      code: 500,
      message: message,
    });
  }

  export default{
    commonResponse,
    failure_response
  }