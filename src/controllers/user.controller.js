import user_table_services from "../db_services/user.table.services.js";
import param_table_services from "../db_services/param.table.services.js";
import user_services from '../services/user.services.js'
import user_param_services from "../db_services/user.param.table.service.js";


import lib from '../common/lib.js'
import user from "../models/user.tables.js";

let user_controllers = {
    // add_user: async (req, res) => {
    //     try {
    //         let { userID, userParams } = req.body
    //         userParams = JSON.parse(userParams)
    //         await user_table_services.add_user({ userID }).then(async (result) => {
    //             Object.keys(userParams).forEach(async (param) => {
    //                 let param_exit = await user_services.check_param_exsist(param)
    //                 if (param_exit) {
    //                           console.log("here");
    //                 }
    //                 else {
    //                     await param_table_services.add_param({ param }).then((data) => {
    //                     }).catch((error) => {
                            
                            
    //                     })

    //                 }
    //             })
    //         }).catch(async (error) => {
    //             await response(res, "bad_requset", {}, error.errors[0].message)
    //         })


    //     } catch (error) {
    //         console.log(error);

    //     }


    // },
    add_user: async (req, res) => {
        try {

            let { externalUser_ID, userParams } = req.body
            let system_ID = await user_table_services.getlastsystemid()
            console.log(system_ID)
            let result = await user_table_services.add_user({ system_ID, externalUser_ID });
            console.log("sycbih",result.respStatus)
            if (result.respStatus=== true) {
                if(userParams){
                let userID = result.result.user_ID
                Object.keys(userParams).forEach(async (param_Name) => {
                    let param_exit = await user_services.check_param_exsist(param_Name)
                    let paramid = '';
                    if (param_exit === undefined || param_exit === null) {
                        let result = await param_table_services.add_param({ param_Name })
                        paramid = result.param_ID;
                    } else {
                        let { dataValues } = param_exit;
                        paramid = dataValues.param_ID;
                    }
                    if (paramid) {
                        let paramID = paramid;
                        let value = userParams[param_Name];
                        let status = true;
                       await user_param_services.add_user_param({ userID, paramID, value, status });
                    }
                })

                return  lib.commonResponse(res,200,"New User Added Successfully")
            }   
            return lib.commonResponse(res,200,"New User Added Successfully")
            }else {
                return lib.commonResponse(res,401,result.errorString)
              }
        } catch (error) {
            console.log(error);
            return lib.failure_response(res, 500, "Unable to process.")
        }
    },


    addParams:async (req, res) => {
    try{
    let { externalUser_ID, userParams } = req.body
    if(externalUser_ID){
        let check_exist = await user_table_services.get_user(externalUser_ID)
        if(check_exist){
         let userID = check_exist.user_ID
         if(userParams){    
            Object.keys(userParams).forEach(async (param_Name) => {
                let param_exit = await user_services.check_param_exsist(param_Name)
                let paramid = '';
                if (param_exit === undefined || param_exit === null) {
                    let result = await param_table_services.add_param({ param_Name })
                    paramid = result.param_ID;
                    
                } else {
                    let { dataValues } = param_exit;
                    paramid = dataValues.param_ID;
               
                }
                if (paramid) {
                    //check user param table if userid +p aram id exisit
                    //if yes update valude or add value
                    let paramID = paramid;
                    let value = userParams[param_Name];
                    //let userID = user_ID
                   
                   let result = await user_param_services.getmatchdata(userID,paramID)
                   if(result){
                    console.log("exist");
                    console.log('param_Name>>>>>',param_Name)
                    console.log('paramID>>>>>',paramID)
                    console.log('value>>>>>',value)
                    console.log('userID>>>>>',userID)
                    await user_param_services.updateUserParam(value,userID,paramID)
                   }else{
                    let status = true;
                   
                   
                    await user_param_services.add_user_param({ userID, paramID, value, status });

                   }
                    
                   
                }

            })

            return  lib.commonResponse(res,200,"New Params Added Successfully")
         }else{
            return lib.commonResponse(res,400,"UserParams is required field")
         }
            }else {
            return lib.commonResponse(res,401,"externalUser_ID Doesnot Exist")
         }
      
        }else {
            return lib.commonResponse(res,400,"externalUser_ID is required field")
        }
    
    } catch (error) {
        console.log(error);
        return lib.failure_response(res, 500, "Unable to process.")
    }
}

}

export default user_controllers