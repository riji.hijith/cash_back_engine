import { Sequelize,DataTypes} from "sequelize";
import config from "./config.js";

let { db_name, user, password, host, dialect } = config.sequelize

const db = new Sequelize(db_name, user, password, { host, dialect })


export { db, Sequelize,DataTypes}