import params from "../models/param.tables.js";


let param_table_services = {

    get_param: async (match) => {
        return new Promise(async(resolve, reject) => {
            try {
             let result = await params.findOne({ where: match })
             resolve(result)
            } catch (error) {
                reject(error)
            }
        })

    },

    add_param: async (param_name) => {
        return new Promise(async(resolve, reject) => {
        try {
            
            let result =  await params.create(param_name)
            resolve(result)
           
        } catch (error) {
            reject(error)
        }
    })
}

}

export default param_table_services