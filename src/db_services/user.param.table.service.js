import user_param_table from "../models/user.param.table.js";
import { Sequelize, db, DataTypes } from "./../sequalize.connection.js"


let user_param_services = {

    add_user_param: async (data) => {
        
        try {
            let respStatus = true
           let result = await user_param_table.create(data)
            return {respStatus, result}
        } catch (error) {
            console.log(error)
            let respStatus = false;
            let errorString=[];
            let{errors} = error;
            errors.forEach(async (err) => {                
                errorString.push(err['message']);
             });
          
            return {respStatus,errorString}
        }
    
    },

    getmatchdata:async(userID,paramID) => {
        return new Promise (async(resolve, reject) => {
        try {
            let result =  await user_param_table.findOne({where:{userID,paramID}})
            console.log("result",result);
            resolve(result) 
        } catch (error) {
           reject(error)
        }
    })

    },
    updateUserParam: async (value,userID,paramID) => {
        return new Promise(async (resolve, reject) => {
            try {

                console.log('paramID2>>>>>',paramID)
                console.log('value2>>>>>',value)
                console.log('userID2>>>>>',userID)
                    // return  await db.query("SELECT *from user_tables ORDER BY user_ID DESC LIMIT 1")
            // return await db.query("UPDATE user_param_tables SET value = {value} WHERE userID = {userID} AND paramID = {paramID} ")
                let result = await user_param_table.update({ value: value }, {
                    where: { 
                        userID: userID, 
                        paramID: paramID 
                    }
                })
                resolve(result) 
            } catch (error) {
                reject(error)
                //throw error
            }

        })
    }

}

export default user_param_services