import user from "../models/user.tables.js";

import { Sequelize, db, DataTypes } from "./../sequalize.connection.js"



let user_table_services = {

    add_user: async ( /* user_details must be an object { userID:string}*/user_details) => {
        try {
            let respStatus = true
            let result = await user.create(user_details);
            return {respStatus, result}
            //return await user.create(user_details)
        } catch (error) {
            console.log(error)
            let respStatus = false;
            let errorString=[];
            let{errors} = error;
            errors.forEach(async (err) => {                
                errorString.push(err['message']);
             });
            return {respStatus,errorString}
        }
    },

    get_user: async (/** match must be an type of object */externalUser_ID) => {
        
        return new Promise(async (resolve, reject) => {
            try {
           
                let result =await  user.findOne({ where: {externalUser_ID} })
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
       
    },

    getlastsystemid: async ()=>{
        try {
           // return  await db.query("SELECT *from user_tables ORDER BY user_ID DESC LIMIT 1")
            
          let result = await user.findOne({order: [ [ 'createdAt', 'DESC' ]],});
          let id = result.system_ID+1;
           return id
            } catch (error) {
            throw error
        }
    }

}



export default user_table_services