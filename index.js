// importing npm modules
import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import { db } from './src/sequalize.connection.js'
import cors from "cors";
import morgan from "morgan";
import config from "./src/config.js";
import route from "./src/routes.js";

// creating instance for express
const app = express();
dotenv.config();

global.appRoot = process.cwd();

// global middleware
app.use(
    express.urlencoded({
        parameterLimit: 100000,
        limit: "100mb",
        extended: true,
    })
);
app.use(
    express.json({
        limit: "100mb",
    })
);
app.use(
    morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(cors());
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Headers", "*");
    res.setHeader("Access-Control-Allow-Methods", "*");
    res.setHeader("Access-Control-Allow-Origin", "*");
    next();
});


await db
    .authenticate()
    .then(() => console.log("Connection has been established successfully."))
    .catch((error) => console.error("Unable to connect to the database:", error));

await db.sync()

// await user_parameters_table.create_user_parameters_table()
app.use('/api',route)

app.get("*", (req, res) => {
    res.status(404).json({
        status: false,
        message: "Unknown path...",
    });
});
app.post("*", (req, res) => {
    res.status(404).json({
        status: false,
        message: "Unknown path...",
    });
});

const PORT = config.app.PORT || 8080;

app.listen(PORT, (err) => {
    if (err) console.log(`server error.`);
    else console.log(`server is on ${PORT}`);
});  




